﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;

namespace Lacuna.Pki.EntityFrameworkConnector {

	public class EntityFrameworkLogger : ILogger {

		public static void Configure(IPkiLogContext context, LogLevels minLevel = LogLevels.Info) {
			PkiConfig.Logger = new EntityFrameworkLogger(context, minLevel);
		}

		private Thread thread;
		private ConcurrentQueue<Tuple<LogLevels, string, string>> queue;
		private DbContext dbContext;
		private DbSet<PkiLogEntry> dbSet;
		private LogLevels minLevel;
		private AutoResetEvent enqueueEvent;
		private AutoResetEvent flushEvent;
		private AutoResetEvent flushCompleteEvent;

		public EntityFrameworkLogger(IPkiLogContext dbContext, LogLevels minLevel = LogLevels.Info) {

			if (dbContext == null) {
				throw new ArgumentNullException();
			}

			this.dbContext = (dbContext as DbContext);
			if (this.dbContext == null) {
				throw new ArgumentException("The dbContext argument must be a class that derives from System.Data.Entity.DbContext");
			}

			this.dbSet = dbContext.PkiLog;
			this.minLevel = minLevel;
			this.queue = new ConcurrentQueue<Tuple<LogLevels, string, string>>();
			this.enqueueEvent = new AutoResetEvent(false);
			this.flushEvent = new AutoResetEvent(false);
			this.flushCompleteEvent = new AutoResetEvent(false);
			this.thread = new Thread(work) {
				IsBackground = true
			};
			this.thread.Start();
		}

		public void Log(LogLevels level, string message, string source) {
			if (level < minLevel) {
				return;
			}
			queue.Enqueue(new Tuple<LogLevels, string, string>(level, message, source));
			enqueueEvent.Set();
		}

		private void work() {

			int unflushedEntries = 0;
			Tuple<LogLevels, string, string> entry;

			while (true) {

				int eventSignalled;
				if (unflushedEntries == 0) {
					// wait indefinetly
					eventSignalled = WaitHandle.WaitAny(new WaitHandle[] { enqueueEvent, flushEvent });
				} else {
					// wait for 1 second, if nothing happens we'll flush
					eventSignalled = WaitHandle.WaitAny(new WaitHandle[] { enqueueEvent, flushEvent }, TimeSpan.FromSeconds(1));
				}
				var flushRequested = (eventSignalled == 1);
				var timedOut = (eventSignalled == WaitHandle.WaitTimeout);

				// empty queue whatever the case
				while (queue.TryDequeue(out entry)) {
					dbSet.Add(PkiLogEntry.Create(entry.Item1, entry.Item2, entry.Item3));
					++unflushedEntries;
				}

				// save changes if any of the following holds:
				// - flush requested
				// - timeout occurred (which means there are unflushedEntries and we're idle)
				// - unflushedLogs >= 10
				if (flushRequested || timedOut || unflushedEntries >= 10) {
					dbContext.SaveChanges();
					unflushedEntries = 0;
				}

				// signal flush completed if flush requested
				if (flushRequested) {
					flushCompleteEvent.Set();
				}
			}
		}

		public void Flush() {
			flushEvent.Set();
			flushCompleteEvent.WaitOne();
		}
	}
}
