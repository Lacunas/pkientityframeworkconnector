﻿using Lacuna.Pki.Stores;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacuna.Pki.EntityFrameworkConnector {

	public class EntityFrameworkNonceStore : INonceStore {

		private DbContext dbContext;
		private IDbSet<PkiNonce> dbSet;
		private bool autoSaveChanges;
		
		public EntityFrameworkNonceStore(IPkiNonceStoreContext dbContext, bool autoSaveChanges = true) {
			
			if (dbContext == null) {
				throw new ArgumentNullException();
			}

			this.dbContext = (dbContext as DbContext);
			if (this.dbContext == null) {
				throw new ArgumentException("The dbContext argument must be a class that derives from System.Data.Entity.DbContext");
			}

			this.dbSet = dbContext.PkiNonces;
			this.autoSaveChanges = autoSaveChanges;
		}

		public void Store(byte[] nonce) {
			dbSet.Add(new PkiNonce() { Nonce = nonce });
			if (autoSaveChanges) {
				dbContext.SaveChanges();
			}
		}

		public bool CheckAndRemove(byte[] nonce) {
			var storedNonce = dbSet.FirstOrDefault(n => n.Nonce == nonce);
			if (storedNonce == null) {
				return false;
			} else {
				dbSet.Remove(storedNonce);
				if (autoSaveChanges) {
					dbContext.SaveChanges();
				}
				return true;
			}
		}
	}
}
