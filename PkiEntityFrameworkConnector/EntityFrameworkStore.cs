﻿using Lacuna.Pki.Stores;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacuna.Pki.EntityFrameworkConnector {

	public class EntityFrameworkStore : ISimpleStore {

		private IDbSet<PkiStoreObject> dbSet;

		public EntityFrameworkStore(IPkiStoreContext dbContext) {
			if (dbContext == null) {
				throw new ArgumentNullException();
			}
			this.dbSet = dbContext.PkiStore;
		}

		public EntityFrameworkStore(IDbSet<PkiStoreObject> dbSet) {
			if (dbSet == null) {
				throw new ArgumentNullException();
			}
			this.dbSet = dbSet;
		}

		public byte[] Get(byte[] index) {
			var obj = dbSet.FirstOrDefault(o => o.Index == index);
			if (obj == null) {
				return null;
			}
			return obj.Content;
		}

		public void Put(byte[] index, byte[] content) {
			dbSet.Add(new PkiStoreObject() {
				Index = index,
				Content = content
			});
		}
	}

}
