﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacuna.Pki.EntityFrameworkConnector {

	public interface IPkiLogContext {
		DbSet<PkiLogEntry> PkiLog { get; set; }
	}

	[Table("LacunaPkiLog")]
	public class PkiLogEntry {

		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

		public DateTime UTCDate { get; set; }
		public LogLevels Level { get; set; }
		public string Source { get; set; }
		public string Message { get; set; }

		public PkiLogEntry() {
		}

		internal static PkiLogEntry Create(LogLevels level, string message, string source) {
			return new PkiLogEntry() {
				UTCDate = DateTime.UtcNow,
				Source = source,
				Level = level,
				Message = message
			};
		}
	}
}
