﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacuna.Pki.EntityFrameworkConnector {

	public interface IPkiNonceStoreContext {
		DbSet<PkiNonce> PkiNonces { get; set; }
	}

	[Table("LacunaPkiNonces")]
	public class PkiNonce {
		[Key, MinLength(16), MaxLength(16)]
		public byte[] Nonce { get; set; }
	}

}
