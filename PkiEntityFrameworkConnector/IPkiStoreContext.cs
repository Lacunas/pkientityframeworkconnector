﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacuna.Pki.EntityFrameworkConnector {

	public interface IPkiStoreContext {
		DbSet<PkiStoreObject> PkiStore { get; set; }
	}

	[Table("LacunaPkiStore")]
	public class PkiStoreObject {

		[Key, MinLength(32), MaxLength(32)]
		public byte[] Index { get; set; }

		[Required]
		public byte[] Content { get; set; }

	}
}
