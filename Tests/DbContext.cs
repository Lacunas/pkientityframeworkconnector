﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacuna.Pki.EntityFrameworkConnector.Tests {
	
	public class DbContext : System.Data.Entity.DbContext, IPkiLogContext, IPkiStoreContext, IPkiNonceStoreContext {

		public DbSet<PkiLogEntry> PkiLog { get; set; }
		public DbSet<PkiStoreObject> PkiStore { get; set; }
		public DbSet<PkiNonce> PkiNonces { get; set; }

		public DbContext(): base("Local") {

		}

	}
}
