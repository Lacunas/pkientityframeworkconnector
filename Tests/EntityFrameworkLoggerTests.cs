﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Threading.Tasks;

namespace Lacuna.Pki.EntityFrameworkConnector.Tests {
	
	[TestClass]
	public class EntityFrameworkLoggerTests {

		[TestInitialize]
		public void Initialize() {
			using (var dbContext = new DbContext()) {
				dbContext.Database.ExecuteSqlCommand("TRUNCATE TABLE [LacunaPkiLog]");
			}
		}

		[TestMethod]
		public void SingleThreadTest() {

			var iterations = 1000;

			EntityFrameworkLogger.Configure(new DbContext());
			var sw = Stopwatch.StartNew();
			for (int i = 0; i < iterations; i++) {
				PkiConfig.Logger.Log(LogLevels.Info, "Message #" + i, "Lacuna.Pki.xxx");
			}
			sw.Stop();
			PkiUtil.FlushLog();

			// Execution of the loop should be nearly instantaneous, since we've specified asynchronous logging (excluding the call to Finalize, which flushes the log) 
			Assert.IsTrue(sw.Elapsed < TimeSpan.FromMilliseconds(100));

			var indexes = new List<int>();
			var regex = new Regex(@"Message #(\d+)");

			using (var dbContext = new DbContext()) {
				foreach (var logEntry in dbContext.PkiLog) {
					var match = regex.Match(logEntry.Message);
					if (match.Success) {
						indexes.Add(int.Parse(match.Groups[1].Value));
					}
				}
			}

			Assert.AreEqual(iterations, indexes.Count);
			Enumerable.Range(0, iterations).ToList().ForEach(i => Assert.IsTrue(indexes.Contains(i)));
		}

		[TestMethod]
		public void MultiThreadTest() {

			var iterations = 1000;

			EntityFrameworkLogger.Configure(new DbContext());
			var sw = Stopwatch.StartNew();
			Parallel.For(0, iterations, i => PkiConfig.Logger.Log(LogLevels.Info, "Message #" + i, "Lacuna.Pki.xxx"));
			sw.Stop();
			PkiUtil.FlushLog();

			// Execution of the loop should be nearly instantaneous, since we've specified asynchronous logging (excluding the call to Finalize, which flushes the log) 
			Assert.IsTrue(sw.Elapsed < TimeSpan.FromMilliseconds(100));

			var indexes = new List<int>();
			var regex = new Regex(@"Message #(\d+)");

			using (var dbContext = new DbContext()) {
				foreach (var logEntry in dbContext.PkiLog) {
					var match = regex.Match(logEntry.Message);
					if (match.Success) {
						indexes.Add(int.Parse(match.Groups[1].Value));
					}
				}
			}

			Assert.AreEqual(iterations, indexes.Count);
			Enumerable.Range(0, iterations).ToList().ForEach(i => Assert.IsTrue(indexes.Contains(i)));
		}
	}
}
