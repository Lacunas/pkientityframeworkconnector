﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Lacuna.Pki.Stores;

namespace Lacuna.Pki.EntityFrameworkConnector.Tests {

	[TestClass]
	public class EntityFrameworkNonceStoreTests {

		[TestMethod]
		public void ValidateAndTryReplay() {

			var pfxContent = File.ReadAllBytes("Fermat.pfx");
			var pfxStore = Pkcs12CertificateStore.Load(pfxContent, "");
			var cert = pfxStore.GetCertificatesWithKey().First();
			var trustArbitrator = new TrustedRoots(cert.Certificate.TrustAnchor);

			byte[] nonce;
			using (var dbContext = new DbContext()) {
				var certAuth = new PKCertificateAuthentication(new EntityFrameworkNonceStore(dbContext));
				nonce = certAuth.Start();
			}

			var signature = cert.SignData(PKCertificateAuthentication.DigestAlgorithm, nonce);

			ValidationResults vr;
			PKCertificate authenticatedCert;
			using (var dbContext = new DbContext()) {
				var certAuth = new PKCertificateAuthentication(new EntityFrameworkNonceStore(dbContext));
				vr = certAuth.Complete(nonce, cert.Certificate.EncodedValue, signature, trustArbitrator, out authenticatedCert);
			}

			Assert.IsTrue(vr.IsValid);
			Assert.AreEqual(cert.Certificate, authenticatedCert);

			using (var dbContext = new DbContext()) {
				var certAuth = new PKCertificateAuthentication(new EntityFrameworkNonceStore(dbContext));
				vr = certAuth.Complete(nonce, cert.Certificate.EncodedValue, signature, trustArbitrator, out authenticatedCert);
			}

			Assert.IsFalse(vr.IsValid);
			Assert.AreEqual(cert.Certificate, authenticatedCert);
		}


	}
}
