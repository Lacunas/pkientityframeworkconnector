﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Lacuna.Pki.Cades;

namespace Lacuna.Pki.EntityFrameworkConnector.Tests {

	[TestClass]
	public class EntityFrameworkStoreTests {

		[TestInitialize]
		public void TestInitialize() {
			using (var dbContext = new DbContext()) {
				dbContext.Database.ExecuteSqlCommand("TRUNCATE TABLE [LacunaPkiStore]");
			}
		}

		[TestMethod]
		public void CompressAndDecompress() {

			for (int i = 1; i <= 2; i++) {

				byte[] precomputedSignature = File.ReadAllBytes(String.Format("Signature{0}.p7s", i));
				
				byte[] compressedSignature;
				using (var dbContext = new DbContext()) {
					var store = new EntityFrameworkStore(dbContext);
					compressedSignature = CadesSignatureCompression.Compress(precomputedSignature, store);
					dbContext.SaveChanges();
				}

				byte[] decompressedSignature;
				using (var dbContext = new DbContext()) {
					var store = new EntityFrameworkStore(dbContext);
					decompressedSignature = CadesSignatureCompression.Decompress(compressedSignature, store);
				}

				CollectionAssert.AreEqual(precomputedSignature, decompressedSignature);

				// Both signatures contain the same 3 certificates and 2 CRLs, so the bucket folder should
				// have only 5 objects, any more than that means unecessary redundancy

				using (var dbContext = new DbContext()) {
					Assert.AreEqual(5, dbContext.PkiStore.Count());
				}
			}
		}
	}
}
