﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lacuna.Pki.EntityFrameworkConnector.TestsWithLatestDependencies {
	
	[TestClass]
	public class EntityFrameworkLoggerTests : Lacuna.Pki.EntityFrameworkConnector.Tests.EntityFrameworkLoggerTests {

		[TestInitialize]
		public void Initialize_Latest() {
			base.Initialize();
		}

		[TestMethod]
		public void SingleThreadTest_Latest() {
			base.SingleThreadTest();
		}

		[TestMethod]
		public void MultiThreadTest_Latest() {
			base.MultiThreadTest();
		}
	}
}
