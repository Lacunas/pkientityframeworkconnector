﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacuna.Pki.EntityFrameworkConnector.TestsWithLatestDependencies {

	[TestClass]
	public class EntityFrameworkNonceStoreTests : Lacuna.Pki.EntityFrameworkConnector.Tests.EntityFrameworkNonceStoreTests {

		[TestMethod]
		public void ValidateAndTryReplay_Latest() {
			base.ValidateAndTryReplay();
		}

	}
}
