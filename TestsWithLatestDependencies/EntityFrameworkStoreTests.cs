﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lacuna.Pki.EntityFrameworkConnector.TestsWithLatestDependencies {
	
	[TestClass]
	public class EntityFrameworkStoreTests : Lacuna.Pki.EntityFrameworkConnector.Tests.EntityFrameworkStoreTests {

		[TestInitialize]
		public void TestInitialize() {
			base.TestInitialize();
		}

		[TestMethod]
		public void CompressAndDecompress_Latest() {
			base.CompressAndDecompress();
		}

	}
}
